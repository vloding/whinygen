#ifndef _MERSENNE_H
#define _MERSENNE_H

#include <assert.h>
#include <stdint.h>
#include <stdio.h>

/*
 *
 * w := number of bits of generated output
 * n := degree of recurrence
 * m := middle word, an offset            x,1 <= m < n
 * r := separation point of one word      0 <= r <= w - 1
 *	a := coefficients of the rational normal form twist matrix
 *
 * 2^(nw-r)-1 must be a mersenne prime
 *
 * https://en.wikipedia.org/wiki/Mersenne_Twister
 *
 */

#define MERSENNE_w 32
#define MERSENNE_n 624
#define MERSENNE_m 397
#define MERSENNE_r 31

#define MERSENNE_a 0x9908B0DFUL

#define MERSENNE_u 11
#define MERSENNE_d 0xFFFFFFFFUL

#define MERSENNE_s 7
#define MERSENNE_b 0x9D2C5680UL

#define MERSENNE_t 15
#define MERSENNE_c 0xEFC60000UL

#define MERSENNE_l 18

#define MERSENNE_f 1812433253UL

#define UPPER_MASK 0x80000000UL /* most significant w-r bits */
#define LOWER_MASK 0x7fffffffUL /* least significant r bits */

void seed_mt(uint32_t seed);
uint32_t extract_number(void);
void twist(void);
uint32_t generate_seed(void);

#endif // !_MERSENNE_H
