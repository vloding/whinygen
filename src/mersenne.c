#include "mersenne.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

// Create a length n array to store the state of the generator
static uint32_t MT[MERSENNE_n];
static size_t m_index = MERSENNE_n + 1;

void seed_mt(uint32_t seed)
{
  m_index = MERSENNE_n;
  MT[0] = seed;
  for (register size_t i = 1; i < MERSENNE_n - 1; i++) {
    MT[i] =
      (MERSENNE_f * (MT[i - 1] ^ (MT[i - 1] >> (MERSENNE_w - 2))) +
       i);
  }
}

// Extract a tempered value based on MT[m_index]
// calling twist() every n numbers
uint32_t extract_number(void)
{
  if (m_index >= MERSENNE_n) {
    if (m_index > MERSENNE_n) {
      fprintf(stderr, "ERROR: Mersenne generator was never seeded\n");
      return 0;
    }
    twist();
  }

  uint32_t y = MT[m_index];
  y = y ^ ((y >> MERSENNE_u) & MERSENNE_d);
  y = y ^ ((y << MERSENNE_s) & MERSENNE_b);
  y = y ^ ((y << MERSENNE_t) & MERSENNE_c);
  y = y ^ (y >> MERSENNE_l);

  m_index = m_index + 1;

  return y;
}

// Generate the next n values from the series x_i
void twist(void)
{
  for (register size_t i = 0; i < MERSENNE_n - 1; i++) {
    uint32_t x =
      (MT[i] & UPPER_MASK) + (MT[(i + 1) % MERSENNE_n] & LOWER_MASK);
    uint32_t xA = x >> 1;
    if ((x % 2) != 0) { // lowest bit of x is 1
      xA = xA ^ MERSENNE_a;
    }
    MT[i] = MT[(i + MERSENNE_m) % MERSENNE_n] ^ xA;
  }

  m_index = 0;
}

uint32_t generate_seed(void)
{
  uint32_t seed;
  void *ptr;

  FILE *random = fopen("/dev/urandom", "rb");
  if (random) {
    fread(&seed, sizeof(seed), 1, random);
    fclose(random);
  } else {
    ptr = malloc(1);
    seed = (uintptr_t) ptr;
    free(ptr);
  }

  return seed;
}
