#include "white_noise.h"
#include "mersenne.h"

#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>

typedef struct state {
  bool high;
  size_t sample;
  int16_t prev_sample;
} state_t;

static state_t state = {
  .high = false,
  .sample = 0,
  .prev_sample = 0,
};

static void (*callbacks[N_CALLBACKS])(int16_t *, size_t, audioData_t*) = {
  [WHITE_NOISE_PASSIVE] = white_noise_passive_filter,
  [WHITE_NOISE_ACTIVE] = white_noise_active_filter,
  [SQUARE_WAVE] = square_wave,
  [SIN_WAVE] = sin_wave,
};

void sdl_audio_callback(void *userdata, Uint8 *stream, int len)
{
  audioData_t *data = userdata;

  assert(len % 2 == 0);

  // white_noise((Uint16*) stream, len);
  // square_wave((Uint16*) stream, len, data->freq);
  callbacks[data->callback]((int16_t *) stream, len, data);
}

void white_noise_passive_filter(int16_t *stream, size_t len, audioData_t *data)
{
  double alpha = M_2PI_FS / (M_2PI_FS + (double) data->freq);

  for (register size_t i = 0; i < len; i++) {
    // state.prev_sample = ((int16_t) (alpha * (double) (VOLUME_MASK(10) & extract_number()) + (1.0 - alpha) * (double) state.prev_sample));
    // state.prev_sample = ((int16_t) (alpha * (double) (VOLUME_MASK(extract_number(), 10)) + (1.0 - alpha) * (double) state.prev_sample));
    state.prev_sample = VOLUME_MASK(((int16_t) (alpha * (double) (extract_number()) + (1.0 - alpha) * (double) state.prev_sample)), VOLUME_SHIFT);
    stream[i] = state.prev_sample;

    // stream[i] = (extract_number() & ((1 << 10) - 1));
  }
}

void white_noise_active_filter(int16_t *stream, size_t len, audioData_t *data)
{
	double beta = 1.0 / (1.0 + M_2PI_FT_FS(data->freq));
	// double alpha = beta * GAIN * M_2PI_FT_FS(data->freq);
	double alpha = beta * data->gain * M_2PI_FT_FS(data->freq);
	// double alpha = (data->gain * M_2PI_FT_FS(data->freq)) / (1.0 + M_2PI_FT_FS(data->freq));

	// printf("alpha: %lf\n", alpha);
	// printf("beta: %lf\n", beta);
	// printf("prev: %hd\n", state.prev_sample);
	// printf("\v");

  for (register size_t i = 0; i < len; i++) {
    // state.prev_sample = - ((int16_t) (alpha * (double) (VOLUME_MASK(10) & extract_number()) + beta * (double) state.prev_sample));
    state.prev_sample = VOLUME_MASK(((int16_t) (alpha * (double) (extract_number()) + beta * (double) state.prev_sample)), 0);
    stream[i] = state.prev_sample;

    // stream[i] = (extract_number() & ((1 << 10) - 1));
  }
}

void square_wave(int16_t *stream, size_t len, audioData_t *data)
{
  size_t sampleN = SAMP_FREQ / (2 * data->freq);

  for (register size_t i = 0; i < len; i++) {
    stream[i] = (1 << 10) * (state.high * 2 - 1);
    if (state.sample++ >= sampleN) {
      state.high = !state.high;
      state.sample = 0;
    }
  }
}

void sin_wave(int16_t *stream, size_t len, audioData_t *data)
{
  size_t sampleN = SAMP_FREQ / data->freq;

  for (register size_t i = 0; i < len; i++) {
    stream[i] = (int16_t) (1 << 10) *
                sin(F_I_DIVISION(state.sample, sampleN) * M_PI * 2);
    if (state.sample++ >= sampleN) {
      state.sample = 0;
    }
  }
}
