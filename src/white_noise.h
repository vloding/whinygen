#ifndef _WHITE_NOISE_H
#define _WHITE_NOISE_H

#include <SDL.h>
#include <stddef.h>

#ifndef M_PI
#    define M_PI 3.14159265358979323846
#endif

#define F_I_DIVISION(a, b) (((double) a) / ((double) b))

#define SAMP_FREQ 48000
#define M_2PI_FS (2.0 * M_PI * (double) SAMP_FREQ)
// #define VOLUME_MASK (0b1000001111111111)
// #define VOLUME_MASK(shift) (((1 << shift) - 1) | 1 << 15)
#define VOLUME_MASK(sound, shift) ((sound) >> (shift))

#define VOLUME_SHIFT 3

#define GAIN 4
#define M_2PI_FT_FS(freq) ((double) (2.0 * M_PI * F_I_DIVISION(freq, SAMP_FREQ)))
// #define ACTIVE_DENOM(freq) ((double) (1.0 + M_2PI_FT_FS(freq)))

enum Audio_Callback {
	WHITE_NOISE_PASSIVE,
	WHITE_NOISE_ACTIVE,
	SQUARE_WAVE,
	SIN_WAVE,
	N_CALLBACKS,
};

typedef struct audio_data {
	size_t freq;
	size_t callback;
	double gain;
} audioData_t;

void sdl_audio_callback(void *userdata, Uint8 *stream, int len);
void white_noise_passive_filter(int16_t *stream, size_t len, audioData_t *data);
void white_noise_active_filter(int16_t *stream, size_t len, audioData_t *data);
void square_wave(int16_t *stream, size_t len, audioData_t *data);
void sin_wave(int16_t *stream, size_t len, audioData_t *data);

#endif // ! _WHITE_NOISE_H
