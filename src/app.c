#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <SDL.h>

#include "app.h"
#include "mersenne.h"
#include "white_noise.h"

static SDL_AudioDeviceID device;
static enum
{
  PLAYING = 0,
  PAUSED = 1,
} device_state;

void audio_pause(GtkWidget *widget, gpointer data)
{
  (void) widget;
  (void) data;

  device_state = !device_state;

  SDL_PauseAudioDevice(device, device_state);
}

void set_callback_white_noise_passive(GtkWidget *widget,
                                      gpointer user_data)
{
  (void) widget;
  ((audioData_t *) user_data)->callback = WHITE_NOISE_PASSIVE;
}

void set_callback_white_noise_active(GtkWidget *widget,
                                     gpointer user_data)
{
  (void) widget;
  ((audioData_t *) user_data)->callback = WHITE_NOISE_ACTIVE;
}

void set_callback_square_wave(GtkWidget *widget, gpointer user_data)
{
  (void) widget;
  ((audioData_t *) user_data)->callback = SQUARE_WAVE;
}

void set_callback_sin_wave(GtkWidget *widget, gpointer user_data)
{
  (void) widget;
  ((audioData_t *) user_data)->callback = SIN_WAVE;
}

void change_freq(GtkAdjustment *self, gpointer user_data)
{
  ((audioData_t *) user_data)->freq =
    (size_t) gtk_adjustment_get_value(self);
}

void change_gain(GtkAdjustment *self, gpointer user_data)
{
  ((audioData_t *) user_data)->gain =
    (size_t) gtk_adjustment_get_value(self);
}

void update_text(GtkAdjustment *self, gpointer user_data)
{
  GtkEntryBuffer *buffer = gtk_text_get_buffer(user_data);
  int value = (int) gtk_adjustment_get_value(self);

  char text[10];

  snprintf(text, 10, "%d", value);

  gtk_entry_buffer_set_text(buffer, text, 10);
}

static SDL_AudioDeviceID init_SDL(void *user_data)
{
  if (SDL_Init(SDL_INIT_AUDIO) < 0) {
    fprintf(stderr,
            "ERROR: could not initialize SDL: %s\n",
            SDL_GetError());
    // exit(EXIT_FAILURE);
    return 0;
  }

  SDL_AudioDeviceID dev;
  SDL_AudioSpec desired = {
    .freq = SAMP_FREQ,
    .format = AUDIO_S16LSB,
    .channels = 1,
    .callback = sdl_audio_callback,
    .userdata = user_data,
  };

  if ((dev = SDL_OpenAudioDevice(
         NULL, 0, &desired, NULL, SDL_AUDIO_ALLOW_ANY_CHANGE)) == 0)
  {
    fprintf(stderr,
            "ERROR: could not open audio device: %s\n",
            SDL_GetError());
    // exit(EXIT_FAILURE);
    return 0;
  }

  return dev;
}

void activate(GtkApplication *app, gpointer user_data)
{
  GtkWidget *window;
  GtkWidget *button;
  GtkWidget *box;

  seed_mt(generate_seed());
  device = init_SDL(user_data);
  device_state = PLAYING;

  SDL_PauseAudioDevice(device, 0);

  window = gtk_application_window_new(app);
  gtk_window_set_title(GTK_WINDOW(window), "Whinygen");
  gtk_window_set_default_size(GTK_WINDOW(window), 600, 400);

  box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  gtk_widget_set_halign(box, GTK_ALIGN_FILL);
  gtk_widget_set_valign(box, GTK_ALIGN_CENTER);

  button = gtk_button_new_with_label("Play/Pause");
  g_signal_connect(button, "clicked", G_CALLBACK(audio_pause), NULL);
  gtk_box_append(GTK_BOX(box), button);

  button = gtk_button_new_with_label("White Noise Passive Filter");
  g_signal_connect(button,
                   "clicked",
                   G_CALLBACK(set_callback_white_noise_passive),
                   user_data);
  gtk_box_append(GTK_BOX(box), button);

  button = gtk_button_new_with_label("White Noise Active Filter");
  g_signal_connect(button,
                   "clicked",
                   G_CALLBACK(set_callback_white_noise_active),
                   user_data);
  gtk_box_append(GTK_BOX(box), button);

  button = gtk_button_new_with_label("Square Wave");
  g_signal_connect(button,
                   "clicked",
                   G_CALLBACK(set_callback_square_wave),
                   user_data);
  gtk_box_append(GTK_BOX(box), button);

  button = gtk_button_new_with_label("Sin Wave");
  g_signal_connect(
    button, "clicked", G_CALLBACK(set_callback_sin_wave), user_data);
  gtk_box_append(GTK_BOX(box), button);

  GtkAdjustment *adj =
    gtk_adjustment_new(440.0, 20.0, 20000.0, 1.0, 0.0, 0.0);
  GtkWidget *scale = gtk_scale_new(GTK_ORIENTATION_HORIZONTAL, adj);
  GtkWidget *text = gtk_text_new();

  g_signal_connect(
    adj, "value-changed", G_CALLBACK(change_freq), user_data);
  g_signal_connect(
    adj, "value-changed", G_CALLBACK(update_text), text);

  gtk_box_append(GTK_BOX(box), scale);
  gtk_box_append(GTK_BOX(box), text);

  adj = gtk_adjustment_new(4.0, 0.0, 100.0, 1.0, 0.0, 0.0);
  scale = gtk_scale_new(GTK_ORIENTATION_HORIZONTAL, adj);
  text = gtk_text_new();

  g_signal_connect(
    adj, "value-changed", G_CALLBACK(change_gain), user_data);
  g_signal_connect(
    adj, "value-changed", G_CALLBACK(update_text), text);

  gtk_box_append(GTK_BOX(box), scale);
  gtk_box_append(GTK_BOX(box), text);

  gtk_window_set_child(GTK_WINDOW(window), box);

  gtk_widget_show(window);
}
