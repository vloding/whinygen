#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <SDL.h>
#include <gtk/gtk.h>

#include "SDL_audio.h"
#include "app.h"
#include "mersenne.h"
#include "white_noise.h"

//------------------------------------------------------------------//
//                               Main                               //
//------------------------------------------------------------------//

int main(int argc, char *argv[])
{
  (void) argc;
  (void) argv;

  GtkApplication *app;
  int status = 0;

  audioData_t *audio_data = malloc(sizeof(audioData_t));
  audio_data->callback = WHITE_NOISE_PASSIVE;
  audio_data->freq = 440;
  audio_data->gain = 4.0;

  app = gtk_application_new("xyz.vleo.whinygen",
                            G_APPLICATION_FLAGS_NONE);
  g_signal_connect(app, "activate", G_CALLBACK(activate), audio_data);
  status = g_application_run(G_APPLICATION(app), argc, argv);

  SDL_Quit();
  free(audio_data);
  g_object_unref(app);

  return status;
  // return 0;
}
