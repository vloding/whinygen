#ifndef _APP_H
#define _APP_H

#include <gtk/gtk.h>

void audio_pause(GtkWidget *widget, gpointer data);
void set_callback_white_noise(GtkWidget *widget, gpointer data);
void set_callback_square_wave(GtkWidget *widget, gpointer data);
void set_callback_sin_wave(GtkWidget *widget, gpointer data);
void change_freq(GtkAdjustment *self, gpointer user_data);
void change_gain(GtkAdjustment *self, gpointer user_data);
void update_text(GtkAdjustment *self, gpointer user_data);
void activate(GtkApplication *app, gpointer user_data);

#endif // !_APP_H
