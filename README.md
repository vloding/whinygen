### EARING HAZARD WARNING

# Whinygen
A white noise generator.

Make sure to test without headphones when first compiling and running.

# Dependencies
- SDL2
- gtk4
- pkg-config
